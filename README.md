# SOP Zaoczne

Systemy operacyjne - wykład i laborki -zaoczne

## Info o regex dla mnie (TP)

```js
[\[][=]([^\|]*)\|([^\]]*)[\]]   ->   [$2]($1)
[\[]([^\|\[\]]*)\|([^\]]*)[\]]  ->   [$2](data/$1)

[\[][=]([^\]]*)[\]]([^\(]|$)    ->   [$1]($1)$2
[\[]([^\]]*)[\]]([^\(]|$)       ->   [$1](data/$1)$2
```

```
find . | grep '\.pdf' | grep tex | awk -F'/' '{print "* [" $4 "](" $2 "/" $3 "/" $4 ")"}' | sort
```
https://learngitbranching.js.org/
