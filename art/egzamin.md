# Zagadnienia na egzamin

* Jaka jest różnica miedzy ścieżką relatywną, a absolutną? Podaj także przykład.
* Do czego służy licznik otwarć pliku?
* Jaki typ plików jest bezpośrednio obsługiwany przez praktycznie każdy system * operacyjny?
* Co to jest biblioteka współdzielona?
* Z jakiego powodu stosuje się biblioteki współdzielone?
* Wymień jakie znasz metody komunikacji międzyprocesowej.
* Co to jest pamięć współdzielona (shared memory)?
* Co to jest PID?
* Co to jest PPID?
* Do czego służą sygnały?
* Jak wygląda schemat producent - konsument?
* Wyjaśnij (na jakimś przykładzie) sytuację wyścigu. 
* W jaki sposób rozwiązuje się problem sytuacji wyścigu?
* Do czego służy sekcja krytyczna?
* W jaki sposób można zrealizować sekcję mutex?
* Co to jest mutex?
* Co to jest semafor?
* Do czego może służyć semafor?
* Co to jest sytuacja zagłodzenia (w kontekście systemów operacyjnych)?
* Co to jest zakleszczenie? Podaj przykład.
* Opisz jak wygląda problem jedzących filozofów?
* Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść * i podniesie najpierw prawą pałeczkę?
* Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść * i podniesie najpierw lewą pałeczkę?
* Co trzeba zrobić, aby zablokować wyłączenie programu konsolowego w momencie * wciśnięcia Ctrl+C?
* Do czego można wykorzystać obsługę sygnałów?
* Na czym polega ,,problem czytelników i pisarzy''?
* Jak rozumiesz pojęcia pamięci wirtualnej?
* Do czego służy pamięć wirtualna?
* Jakie mechanizmy sprzętowe i programowe umożliwiają obsługę pamięci wirtualnej?
* Co to jest stronicowanie na żądanie?
* Jak można zrealizować stronicowanie na żądanie?
* Na czym polega kopiowanie przy zapisie?
* Co się stanie, jeśli system będzie wykonywał zbyt wiele wymagających dużo pamięci * procesów? Jak się nazywa zjawisko, które wtedy się pojawi?
* Czy kolejka nazwana FIFO (reprezentowana przez plik w systemie plików) będzie * działała na zdalnym systemie plików?
* Kiedy może następować wiązanie adresów?
* Co to jest adres bezwzględny?
* Co to jest adres względny? Co on tworzy?
* Jaka jednostka w komputerze służy do tłumaczenia adresów logicznych na fizyczne?
* W jakich sytuacjach program uruchomiony w systemie operacyjnym korzysta z adresów * fizycznych? % tu jest haczyk - nigdy
* Jakie znasz dwa tryby działania procesora (w kontekście uprawnień i systemów * operacyjnych)?
* Czy proces w trybie użytkownika może zażądać dostępu do dowolnego adresu pamięci?
* Czy proces w trybie systemu może zażądać dostępu do dowolnego adresu pamięci?
* Co to jest stronicowanie?
* Co to jest segmentacja?
* Do czego służy rejestr graniczny? 
* Jaka jest różnica między tablicą statyczną a dynamiczną (w kontekście języka C)?
* Do czego służy program make?
* Jak się ma program do procesu?
* Co się składa na proces?
* Przez jakie stany przechodzi proces?
* Co może zawierać blok kontrolny procesu?
* Jak się ma termin ,,obliczenia równoległe'' do ,,obliczeń współbieżnych''?
* Na czym polega przełączanie kontekstu?
* Czym zajmuje się planista (scheduler)?
* Co to znaczy ,,wyekspediowanie procesu'' (process dispatch)?
* Jaki mechanizm sprzętowy pozwala na przełączanie kontekstu?
* Co to znaczy ,,proces ograniczony przez wejście-wyjście''?
* Co to znaczy ,,proces ograniczony przez procesor''?
* Jakie są etapy prowadzące od kodu źródłowego w C do programu wykonywalnego?
* Co się dzieje w momencie linkowania (w kontekście kompilacji programu do kodu * wykonywalnego)?
* Co to jest zmienna powłoki i do czego może służyć?
* Co to jest powłoka systemu?
* Co to jest język skryptowy?
* Z czego się składa system komputerowy?
* Co to jest system operacyjny?
* Z jakiego powodu system operacyjny jest przydatny? Co umożliwia?
* Do czego służy gniazdo sieciowe?
* Jakie znasz 2 podstawowe rodzaje gniazd w kontekście utrzymania połączenia?
* Co należy wykonać aby program nasłuchiwał na wybranym porcie w przypadku protokołu * TCP/IP?
* Czy komenda fork tworzy proces czy też wątek?
* Jaka jest różnica między wątkami a procesami?

