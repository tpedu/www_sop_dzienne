# SOP 2020 Lato, Dzienne

## Zasady zaliczenia

* 2x kolokwium
* Aktywność na zajęciach/zadania domowe - jako dodatkowe punkty do zaliczenia
* Egzamin. Ocena z egzaminu to (pktProcLab+pktProcEgzam)/2

## Literatura

* A. Silberschatz, P.B.Galvin, G.Gagne: Podstawy Systemów Operacyjnych
* Wojciech Moch, Piotr Pilch: Jak działa Linux. Podręcznik administratora. Wydanie II
* Robert Love: Linux. Programowanie systemowe. Wydanie II
* Wykłady i ćwiczenia

## Wykłady

Będą one aktualizowane w trakcie przedmiotu


* [sop na github](https://github.com/pantadeusz/sop)
* [gniazda (socket) na github](https://github.com/pantadeusz/hello-c-network-sockets)
* [sop_01.pdf](tex/01-os/sop_01.pdf)
* [sop_02.pdf](tex/02-bash/sop_02.pdf)
* [sop_03.pdf](tex/03-bash-2/sop_03.pdf) [wyklad_2_online.tar.gz](data/wyklad_2_online.tar.gz)
* [sop_04.pdf](tex/04-awk-tar-inne/sop_04.pdf) [wyklad_3_online.tar.gz](data/wyklad_3_online.tar.gz) [fib.sh](data/fib.sh)
* [sop_05.pdf](tex/05-c/sop_05.pdf) [wskazniki.c](data/wskazniki.c)
* [sop_06.pdf](tex/06-procesy-teoria/sop_06.pdf), [pid.c](data/pid.c). Uzupełniające [procesy w Linux](https://www.cs.columbia.edu/~junfeng/10sp-w4118/lectures/l07-proc-linux.pdf)
* [sop_07.pdf](tex/07-make/sop_07.pdf) [w7.tar.bz2](data/w7.tar.bz2)
* [sop_08.pdf](tex/08-memory/sop_08.pdf) [w8.tar.bz2](data/w8.tar.bz2) [bin_fopen.c](data/bin_fopen.c) [bin_open.c](data/bin_open.c) [stronicowanie w Linuksie](https://0xax.gitbooks.io/linux-insides/Theory/linux-theory-1.html)
* [sop_09.pdf](tex/09-vmem-fifo/sop_09.pdf) oraz dodatek o [posix](tex/09-appendix-posix/sop_09_posix.pdf)
* [sop_10.pdf](tex/10-shmem-sync/sop_10.pdf)
* [sop_11.pdf](tex/11-signal/sop_11.pdf)
* [sop_12.pdf](tex/12-sockets/sop_12.pdf)
* [sop_13.pdf](tex/13-sharedlibs/sop_13.pdf)
* [sop_14.pdf](tex/14-fs/sop_14.pdf)


## Materiały uzupełniające

* [stronicowanie w Linuksie](https://0xax.gitbooks.io/linux-insides/Theory/linux-theory-1.html)
