# Kolokwium 1 powłoka systemu

To czego należy się spodziewać to między innymi:

* znajomość konsoli, w tym między innymi
  * umiejętność poruszania się po katalogach (ścieżki względne (relatywne) i bezwzględne (absolutne))
  * gdzie co jest (katalogi tymczasowe, domowe, katalog z konfiguracją systemu, ...)
  * przekierowanie I/O
  * uruchamianie w tle (bg, fg, jobs)
  * uprawnienia
* bash
  * pętle
  * instrukcje warunkowe
  * wielowybór
  * funkcje
* programy
  * sed
  * awk (podstawy)
  * tar
  * wc, echo, man, less, cat, tr, mv, cp, mkdir, rmdir, rm, i inne z podstawowych komend i programów
