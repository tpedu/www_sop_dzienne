# Kolokwium 2 programowanie systemowe

Czego się spodziewać:

* To co było na poprzedim kolokwium (raczej jako ewentualna metoda wysłania rozwiązań)
* Podstawy języka C, w tym:
  * printf, scanf, gets, puts, fopen, fclose, open, close, read, write, fread, fwrite
* gniazda
* fork
* make
* fifo, pipe
* (na 5) współdzielenie zasobów - pamięć współdzielona, mutex
* (na 5) wątki
* (na 5) biblioteki

## Poprzednio było tu

To czego należy się spodziewać to między innymi:

* język C
* elementy
  * fork + wait
  * fifo, pipe
  * obsługa plików
  * mutex
  * pamięć współdzielona
  * sygnały
  * uprawnienia
  * make
* elementy poprzedniego kolokwium
