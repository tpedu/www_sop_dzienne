# Wprowadzenie do Linuksa

Dzisiaj zajmiemy się instalacją dystrybucji Linuksa. Dzięki temu przetrenujemy zupełnie podstawowe kwestie związane z administracją systemu. Zajmiemy się dystrybucją **Debian**

Opowiem na początku o tym, jak należy do tego podejść. Pokażę maszynę wirtualną. Powiem także co to w ogóle jest dystrybucja Linuksa.

## Etapy

Podczas instalacji wymagane jest odpowiedzenie sobie (i instalatorowi) na kilka pytań:

* Pobranie obrazu instalacyjnego [ze strony projektu](https://www.debian.org/CD/netinst/)
* Jak podzielić dysk na partycje
* Jak chcemy ustawić uprawnienia
* Jakie dodatkowe pakiety/oprogramowanie chcemy zainstalować - nie chcemy UI!
* Ewentualnie instalacja sterowników (to najprawdopodobniej nie będzie konieczne).

Niektóre kwestie ustawia się na etapie instalatora, a inne już po zainstalowaniu

## Instalacja i konfiguracja dodatkowego oprogramowania

Aktualizacja listy pakietów:

```bash
apt update
```

Instalacja wybranego pakietu

```bash
apt install #tu nazwa paczki do zainstalowania#
```

## Zadanie domowe na dodatkowy punkt

Dla ryzykantów: Zainstaluj Gentoo. Spraw, aby system po uruchomieniu pokazywał gdzieś Twoje imię.

Jeśli się to Tobie uda, to proszę pokazać na zajęciach. Daje to pewne 3.0 z pierwszego kolokwium.