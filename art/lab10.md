# 10 FIFO

2020-05-07

Dzisiaj postaramy się napisać prosty program chatu działający na jednej, wielodostępowej, maszynie. Program ten będzie działał w następujący sposób:

* uruchamiamy chat z dwoma argumentami: arg1 oraz arg2, są to nazwy kolejek FIFO (potoków nazwanych, pipe-ów)
* Program tworzy swoją kolejkę, do której będzie wysyłał komunikaty
* Program uruchamia proces i dalej działają już 2 procesy.
  * Jeden proces otwiera swoją kolejkę do pisania, odbiera komunikaty ze standardowego wejścia i wysyła je do kolejki
  * Drugi proces otwiera kolejkę arg2 i czyta napływające dane. Wyświetla je natychmiast.

Składniki tego programu:

mkfifo, open, close, read, write, fork, remove

## Etapy

### W terminalu

Zobacz jak działają kolejki FIFO. Jest w systemie komenda mkfifo pozwalająca na stworzenie kolejki (potoku). Zobacz na 2 terminalach jak wgląda wysyłanie i odbieranie z takiej kolejki za pomocą komendy cat.

```bash
mkfifo nazwapotoku
cat > nazwapotoku # na terminalu 1
cat nazwapotoku # na terminalu 2
```

Zobacz co się stanie, jeśli kilka procesów czyta, a jeden pisze do kolejki.

Zobacz co się stanie, jeśli jeden proces czyta, a kilka pisze.

Co się stanie, jeśli zamkniemy kolejkę po stronie zapisującej? Czy zostanie przerwane czytanie, czy też nie?

Zobacz z sąsiadem/sąsiadką z ławki, czy uda się z tego zrobić prosty 'komunikator'. Może należy ustawić jakieś szczególne uprawnienia? Można umówić się na 2 kolejki - każdy uczestnik po jednej.

### W języku C

Postaraj się zrobić to samo w języku C, to znaczy 2 programy, jeden czytający, a drugi piszący do FIFO. Jeden z nich powinien tworzyć potok, a drugi tylko się do niego łączyć.

### Finish

Teraz należałoby połączyć wszystko w całość -- jeden program tworzy 2 procesy, jeden odbiorczy, a drugi nadawczy, tak jak w treści zadania na dzisiaj.

## Zadanie domowe 1

Napisz program:

Niech jeden proces generuje liczby (na przykład wczytane od użytkownika) i przesyła je do kolejnych procesów (na przykład trzech). Proces, który otrzyma liczbę, oblicza liczbę pierwszą najbliższą podanej i wypisuje ją na standardowe wyjście. Zastosuj FIFO oraz fork.

## Zadanie domowe 2

Przekazywanie żetonu. Napisz program, który uruchomi 4 procesy. W procesie p1 będzie tworzony "żeton" o wartości 1. Zostanie on przekazany do procesu p2, zwiększy on jego wartość i przekaże do p3. Kolejne procesy zwiększą wartość żetonu. Na koniec proces p1 wypisze informację o tym co otrzymał. Niech procesy komunikują się przez kolejkę zrobioną poprzez ```pipe```.

```raw
p1 --(1)--> p2
/|\          |
 |           |
(4)         (2)
 |           |
 |          \|/
p4 <--(3)-- p3
```
