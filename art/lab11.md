# 11 Sygnały

## Sygnały

System Linux (oraz kilka innych systemów) obsługuje mechanizm sygnałów 
([sygnały](http://man7.org/linux/man-pages/man7/signal.7.html) oraz [basic signal handling](http://www.gnu.org/software/libc/manual/html_node/Basic-Signal-Handling.html#Basic-Signal-Handling|)).
Są one wykorzystywane do informowania i obsługi zdarzeń typu dzielenie przez zero, dostęp do niedozwolonej komórki pamięci, sygnały użytkownika, przerwanie potoku i wiele innych.

Sygnał można wygenerować komendą kill. Przypominam, że to ta komenda, która służyła do zamykania procesów. Tak na prawdę, kill generuje sygnał, domyślnie sygnał zakończenia procesu (SIGTERM).

Dzisiaj zajmiemy się obsługą sygnałów. Zrobię mały wstęp i postaramy się stworzyć program korzystający z tego mechanizmu.

## Opis

Zobaczmy przykład (wzorowany na [Sigaction](http://www.gnu.org/software/libc/manual/html_node/Sigaction-Function-Example.html) )

```c
#include <signal.h>

// volatile sig_atomic_t flag = 0;
// to jest mniej 
void handler (int signum) {
   ... obsluga sygnalu
}

int main (void) {
  struct sigaction new_action, old_action;
  new_action.sa_handler = handler;
  sigemptyset (&new_action.sa_mask);
  new_action.sa_flags = 0;

  sigaction (SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
    sigaction (SIGINT, &new_action, NULL);
  sigaction (SIGHUP, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
    sigaction (SIGHUP, &new_action, NULL);
  sigaction (SIGTERM, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
    sigaction (SIGTERM, &new_action, NULL);
}
```

## Opis -- wersja łatwiejsza

Teraz łatwiejsza wersja -- zgodna z ANSI C, ale niezbyt przenośna.

```c
#include <signal.h>

void handler (int signum) {
}

int main (void) {
  signal (SIGINT, SIG_IGN);
  signal (SIGUSR1, handler);
}
```

## Zadanie 1

Przygotuj prosty programik, który będzie czekał w pętli nieskończonej do momentu otrzymania sygnału użytkownika (SIGUSR1). W momencie otrzymania tego sygnału program powinien się zakończyć.

## Zadanie 2

Przygotuj program, który będzie pozwalał na uruchomienie tylko jednej swojej instancji. Dodatkowo w momencie próby uruchomienia drugiej instancji będzie powiadamiał już uruchomiony proces o tym.

Można to zrobić tak:

* Program uruchamia się i sprawdza, czy jest plik /tmp/lock
* Jeśli taki plik nie istnieje, wtedy:
  * tworzony jest taki plik. Do niego zapisywany jest PID bieżącego procesu
  * program instaluje obsługę sygnału USR1, która w momencie otrzymania sygnału wypisuje jakiś tekst.
  * program instaluje obsługę sygnału generowanego przez Ctrl-C (poszukaj jaki to sygnał), która w momencie otrzymania tego sygnału skasuje plik /tmp/lock i spowoduje wyjście z pętli nieskończonej (patrz następny punkt)
  * program wykonuje pętlę nieskończoną zawierającą tylko instrukcję sleep.
* Jeśli taki plik istnieje, wtedy:
  * Wczytywana jest zawartość tego pliku -- pid procesu uruchomionego
  * generowany jest sygnał USR1 do procesu o pid takim jak wczytany
  * program kończy pracę

## Zadanie domowe

dodaj do programu chatu możliwość zakończenia za pomocą ctrl+c (z powiadamianiem drógiej strony o zakończeniu)
