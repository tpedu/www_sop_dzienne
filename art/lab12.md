# 12 Gniazda

* [getaddrinfo](http://man7.org/linux/man-pages/man3/getaddrinfo.3.html)
* [przykład z wykładu](https://github.com/pantadeusz/hello-c-network-sockets)
* [Gniazdo](https://pl.wikipedia.org/wiki/Gniazdo_%28telekomunikacja%29)
* [Port](https://pl.wikipedia.org/wiki/Port_protoko%C5%82u)


## O Gniazdach

Poza przykładem na Githubie, umieszczam jeszcze tu trochę opisu. To jest wersja tylko dla IPv4 - ale także będzie działała:

Tworzenie gniazda nasłuchującego:

```c
int sockfd;
unsigned int port = 9999;
int yes=1;
struct sockaddr_in my_addr;
if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket"); return -1;
}
my_addr.sin_family = AF_INET;
my_addr.sin_port = htons(port);
my_addr.sin_addr.s_addr = INADDR_ANY;
memset(&(my_addr.sin_zero), 0, 8);
if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
    perror("bind"); exit(1);
}
if (listen(sockfd, 10) == -1) {
    perror("listen"); return -1;
}
```

Akceptowanie połączenia:

```c
int clientSocket;
struct sockaddr_in remoteAddr;
socklen_t sin_size = sizeof(struct sockaddr_in);
if ((clientSocket = accept(sockfd, (struct sockaddr *)&remoteAddr, &sin_size)) == -1) {
    perror("on error"); return -1;
}
```

Łączenie się poprzez gniazdo do określonego adresu i portu.

```c
struct addrinfo hints;
struct addrinfo *result, *rp;
int sfd, s, j;
size_t len;
memset(&hints, 0, sizeof(struct addrinfo));
hints.ai_family = AF_UNSPEC;    /* IPv4 lub IPv6 */
hints.ai_socktype = SOCK_STREAM;
hints.ai_flags = 0;
hints.ai_protocol = 0;
s = getaddrinfo("127.0.0.1", "9999", &hints, &result);
if (s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(EXIT_FAILURE);
}
// getaddrinfo() zwraca liste jednostronnie wiazana
or (rp = result; rp != NULL; rp = rp->ai_next) {
    sfd = socket(rp->ai_family, rp->ai_socktype,
                 rp->ai_protocol);
   if (sfd != -1) {
      if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
           break;
      } else {
           close(sfd);
      }
   }
}
if (rp == NULL) { // nie udalo sie
    fprintf(stderr, "Blad polaczenia\n");
    exit(EXIT_FAILURE);
}
freeaddrinfo(result);
close(sfd);
```

## Zadanie 1

Korzystając z przykładów (albo samodzielnie, lub na podstawie manuala) przygotuj programy - klient oraz serwer.

* Niech serwer będzie udostępniał wybrany plik.
* Niech klient będzie miał możliwość pobrania wybranego przez klienta pliku.
* Jako uproszczenie przyjmujemy że serwer nie dba o żadne zasady bezpieczeństwa.

Protokół komunikacji niech wygląda tak:

```raw
KLIENT                                  SERWER
długość_nazwy_pliku               >
nazwa_pliku[długość_nazwy_pliku]  >
                                  <   wielkość_pliku
                                  <   paczka_1[wielkość_pliku]
--zamknij połączenie--                --zamknij połączenie--
```

Serwer powinien działać w pętli.

## Zadanie 2

Niech serwer, w sytuacji, jeśli pliku nie udało się otworzyć, wysyła wielkość pliku jako ```-1```.

## Zadanie 3

Niech serwer zakończy w elegancki sposób pracę w momencie naciśnięcia przez użytkownika Ctrl+C. Chodzi o zamknięcie gniazda nasłuchującego.

## Zadanie 4

Niech serwer zapisuje logi do pliku - to znaczy informacje o tym kto się podłączył (jego IP oraz port) oraz o jaki plik poprosił.

## Zadanie domowe 1

Rozwiń/napisz serwer HTTP tak, aby nadawał się do udostępniania strony WWW. Serwer powinien obsługiwać:

* lista zabronionych adresów
* zapis logów zdarzeń (wraz z informacją kto się podłączył)

## Zadanie domowe 2

Dodatkowo niech serwer pozwala na tworzenie wirtualnych hostów. Niech serwer nie blokuje się w sytuacji kiedy jakiś klient przeciąga połączenie (aby nie blokowało ono innych połączeń). Zastosuj ```fork```.
