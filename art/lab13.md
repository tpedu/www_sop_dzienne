# 13 Powtórka i biblioteki współdzielone

(dodatkowe materiały: [=http://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html] )

## Trochę opisu

Należy skompilować program do postaci Position-independent code (PIC)

```bash
gcc -c -Wall -Werror -fpic biblioteka.c
```


Stworzenie biblioteki współdzielonej

```bash
gcc -shared -o libbiblioteka.so biblioteka.o
```

Kompilacja z wykorzystaniem biblioteki współdzielonej:

```bash
gcc program.c -lbiblioteka -L(tu podaj ścieżkę do katalogu z biblioteką)
```

Uruchomienie programu.

(tu jest haczyk)

Jeśli biblioteka nie znajduje się w jednym z katalogów domyślnych (zobacz konfiguracja /etc/ld.so.conf.d/), wtedy można zastosować zmienną LD_LIBRARY_PATH do określenia gdzie znajdują się dodatkowe biblioteki współdzielone.

### Dynamiczne ładowanie bibliotek

Można sprawić, aby nasz program nie potrzebował linkowania do naszej biblioteki w momencie kompilacji. Można zrobić tak, aby biblioteka była ładowana jawnie z kodu programu. Zobacz przykład:

```c
#include <dlfcn.h>
int (*a)(void);
int main() {
	void *myso = dlopen("./liba.so", RTLD_NOW);
	a = dlsym(myso, "a");
	a();
	dlclose(myso);
	return 0;
}
```

Funkcja <tt>a</tt> znajduje się w bibliotece liba.so.

Aby skompilować taki program, wystarczy:

```c
gcc mazwaprogramu.c -ldl
```

## Zadania na dzisiaj

### Zadanie 1

Przygotuj prostą biblioteczkę do obsługi wektorów (wektor jako tablica trójelementowa). Zaimplementuj 3 funkcje: 

* dodaj
* odejmij
* dlugosc

Niech to będzie biblioteka współdzielona w jakimś wybranym przez Ciebie katalogu.
Wymyśl nazwę tej biblioteki. Struktura katalogów projektu:

```raw
.\
  Makefile
  src     \
           *.c
  include \
           *.h
  dist    \
           lib     \
                    *.so
           include \
                    *.h
```

Niech w pliku Makefile będą zadania:

* all -- zbudowanie biblioteki, w wyniku tego w katalogu dist pojawi się katalog include oraz lib. W katalogu lib pojawi się skompilowany .so, a liki .h zostaną skopiowane do include
* dist -- spakowanie wersji dystrybucyjnej za pomocą tar-a
* clean -- skasowanie wszystkich plików tymczasowych (czyli zawartość katalogu dist, oraz wszelkie pliki .o)
* distclean -- skasowanie wszystkich niepotrzebnych plików (pliki .bak, pliki powstałe w wyniku kompilacji i plik spakowany w wyniku dist).

### Zadanie 2

Napisz program korzystający z biblioteki współdzielonej do wektorów. Niech ten program będzie można skompilować bez konieczności posiadania pliku .c albo .o tej biblioteki.

Zastosuj linkowanie do biblioteki w momencie kompilacji.

Przetestuj czy działa.

### Zadanie 3

Teraz napisz program korzystający z tej biblioteki, ale na zasadzie ładowania funkcji za pomocą funkcji dlsym

## Zadanie domowe

Przetrenuj przesyłanie całych plików za pomocą FIFO. Zobacz czy uda się wysłać Twojemu znajomemu cały spakowany plik przez kolejkę FIFO. Możesz też sam sobie wysyłać plik, zobaczyć jak to działa. Może być konieczne przekierowanie strumieni we-wy.
