# 03 Bash 2

* [Bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)

## Bash - wyrażenia warunkowe

Wyrażenia warunkowe w Bash robi się tak:

```bash
if warunek
then
 ....
elif warunek
then
 ...
else
 ...
fi
```

Można oczywiście krócej:

```bash
if warunek; then
 ....
fi
```

Warunki można przedstawiać tradycyjnie w nawiasach kwadratowych:

```bash
if [[ "abc" -ne "$1" ]]; then
  echo "parametr to nie abc"
fi
```

Operatory logiczne to:

* -eq, =   - równoważność
* =~  - dopasowanie wyrażenia regularnego
* -ne - nierówne
* -gt - większe
* -lt - mniejsze
* -e - plik istnieje
* inne (do przetestowania, wyszukania w dokumentacji - patrz [link do dokumentacji](tiswww.case.edu/php/chet/bash/bashref.html#SEC83))

### Nowsza składnia if

Można także użyć warunków w podwójnych nawiasach:

```bash
if (( 1 > 3 )); then
   echo wieksze
else
   echo niewieksze
fi
```

### Argumenty skryptu

```bash
$@    # wszystkie argumenty jako oddzielne elementy
$*    # wszystkie argumenty jako jeden ciąg znaków
$0    # nazwa skryptu
$1    # pierwszy argument
$2, $3, .. # kolejne argumenty
${#@} # liczba argumentów skryptu
```

### Ćwiczenia 1

1. Napisz skrypt, który sprawdzi, czy podano więcej niż jeden parametr tego skryptu. Jeśli tak, to niech wyświetli pierwszy parametr. Jeśli nie, to niech wyświetli komunikat, że nie podano parametrów.
1. Napisz skrypt, który sprawdzi, czy istnieje plik ```config.cfg``` w bieżącym katalogu. Jeśli taki plik istnieje, to niech wypisze jego zawartość na standardowe wyjście. Jeśli nie istnieje, to niech sprawdzi, czy podano argument i wykona plik o nazwie takiej jak wartość argumentu (parametru) skryptu. Jeśli żadne z powyższych nie nastąpiło, to niech wyświetli komunikat o błędzie.
1. Napisz skrypt, który sprawdzi, czy jego nazwa kończy się ```.sh```. Jeśli nie, to niech zmieni swoją nazwę poprzez dopisanie tego rozszerzenia ([rozszerzenie nazwy pliku](https://pl.wikipedia.org/wiki/Rozszerzenie_nazwy_pliku)). Sprawdzenie można zrobić na kilka sposobów, polecam przetestować 2 typowe:
   * dopasowanie do wyrażenia regularnego (to jest bardziej uniwersalny sposób)
   * porównanie ostatnich 3 liter nazwy skryptu
1. Napisz skrypt, który sprawdzi, czy w bieżącym katalogu jest więcej niż 5 plików. Jeśli tak, to wypisze odpowiedni komunikat z informacją że tak jest. Podpowiem:
   * ls - wyświetla listę plików
   * wc - word count - zlicza znaki, słowa i linie

## Zadanie domowe

(na plusa)

1. Przygotuj wyrażenie regularne, które będzie sprawdzało, czy tekst jest poprawnie napisanym polskim imieniem (od wielkiej litery, może mieć tylko litery i zawierać polskie znaki).
1. Przygotuj wyrażenie regularne sprawdzające czy tekst jest kodem pocztowym (cały tekst, czyli zaczyna się od cyfry i kończy się cyfrą). Sprawdź czy działa.
1. Przygotuj wyrażenie regularne sprawdzające e-mail

Przygotuj przykład skryptu, który pobierze jako argument tekst i odpowie na pytanie (korzystając z wyrażeń regularnych powyżej) czy jest to email, czy imię, czy też kod pocztowy.

Ze względu na sytuację proszę o przesyłanie rozwiązań na email. Tytuł emaila: ```SOP Dzienne Zadanie domowe 3```
W treści proszę się podpisać. Nie akceptuję zbyt podobnych rozwiązań.
