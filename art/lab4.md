# 04 Bash 3

2020-03-26

## Pętle

Bash obsługuje pętle for, while i until. Zobacz [pętle w dokumentacji](https://www.gnu.org/software/bash/manual/html_node/Looping-Constructs.html). Podpowiedzi są takie:

```bash
for i in a b c 1 2 3; do
echo $i
done

for ((i = 1; i < 10; i++)); do echo $i; done

while [ true ]; do
  echo "to się nigdy nie skończy..."
  sleep 1
done
```

## Tablice

Bash obsługuje tablice. Wyglądają one jak zwykłe zmienne, ale nie można ich w zwykły sposób eksportować. Zobacz [dokumentacja -- tablice](http://tiswww.case.edu/php/chet/bash/bashref.html#SEC86).

Deklaracja i użycie (przetestuj, lub zerknij do dokumentacji):

```bash
TABLICA=(element1 "element 2" element\ 3)
TABLICA2=("$@")  # tablica na podstawie argumentów wywołania skryptu
echo ${TABLICA[1]}
echo ${TABLICA[*]}
echo ${TABLICA[@]}
echo ${TABLICA[*]}
echo "${!TABLICA[@]}  ${!TABLICA[*]}"
echo ${#TABLICA[@]}
```

Można także działać na tablicach podobnie jak na ciągach znaków:

```bash
echo "przedostatni '${TABLICA[*]: -2:1}'" # przedostatni element
echo "kawalek '${TABLICA[*]: -2:2}'" # dwa ostatnie elementy
echo "poczatek '${TABLICA[*]:0:2}'" # dwa pierwsze elementy
```

## Ćwiczenia 2

1. Napisz skrypt, który będzie wypisywał liczby od 10 do 1 co 2
1. Napisz skrypt, który będzie wypisywał wszystkie argumenty wywołania skryptu (pamiętaj o tym, że argument może w sobie zawierać jakieś spacje), każy w innej linijce. Sprawdź dla ```./nazwaskryptu A "B i c" D inne\ slowa``` .
1. Napisz skrypt, który wypisze listę plików i katalogów bieżącego katalogu poprzedzając każdą nazwę pliku tekstem "Patrzcie Państwo, oto plik: ". Podpowiedź [internal field separator](https://en.wikipedia.org/wiki/Internal_field_separator), albo komenda ```read``` - uwaga - chodzi o poprawną obsługę znaku spacji w nazwach plików.
1. Napisz skrypt, który dla każdego pliku w bieżącym katalogu, który ma rozszerzenie ```.c``` wykona taką komendę:
   * ```cc (tunazwapliku) -o (tunazwaplikubezrozszerzenia)```
   * Oczywiście zamień ```(tunazwapliku)``` na nazwę pliku, a ```(tunazwaplikubezrozszerzenia)``` na nazwę pliku z obciętym rozszerzeniem. Jeśli chcesz przykładowe pliki do potestowania, [oto one:-)](http://lmgtfy.com/?q=example+c+file)
1. Napisz skrypt, który wczyta listę plików do wypisania z wiersza poleceń, a następnie wypisze ich zawartości raz w kolejności od początku do końca, a raz na odwrót. Podpowiedzi:
   * indeksy ujemne tablicy mogą się przydać
   * można wyliczać wartości wyrażeń arytmetycznych
   * przyda się for

## Funkcje

Bash obsługuje funkcje. Funkcję deklaruje się tak:

```bash
function witaj {
  echo "Funkcja ta jest nieuprzejma i nie wita $1"
  # tu opcjonalnie: return 0
}
```

Argumenty są liczone tak, jak przy zwykłym skrypcie (```$1, $2, ...```).

Funkcję wywołuje się po prostu przez nazwę.

Jeśli chcesz zaimportować plik z funkcjami, to można to zrobić tak:

```bash
. ./plikDoZaimportowania
```

Tak właściwie, to taki plik zostanie dołączony w tym miejscu do bieżącego skryptu. To co innego niż uruchomienie skryptu (czyli na pryzkład ```./plikDoUruchomienia```)

## Ćwiczenia

1. Przygotuj skrypt z funkcją, która będzie wypisywała tekst pokolorowany na zadany kolor. Niech funkcja ta przyjmuje dwa argumenty - tekst i kolor. Zobacz ```man console_codes``` oraz składnię komendy ```echo```.

## Zadanie domowe 1

Przygotuj funkcję obliczającą rekurencyjnie ciąg Fibonacciego. Niech wynik zwraca za pomocą return. Czy są jakieś ograniczenia?

## Zadanie domowe 2

Dla ambitnych: Napisz swoją funkcję do autouzupełniania komend. Tu można puścić wodze fantazji :).  Zobacz [funkcja do uzupełniania tekstu](https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion.html)

Tu nie daję więcej podpowiedzi, jest to zadanie na dodatkowego plusa.
