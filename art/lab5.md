# 05 Bash 4

2020-04-02

## Zadanie 1

Napisz skrypt wyświetlający wszystkich użytkowników opisanych w pliku /etc/passwd w taki sposób, że
pojawi się na ekranie tylko login oraz katalog domowy użytkowników.

Proponuję sprawdzić 3 wersje

* z sed
* z wykorzystaniem awk
* (na plusa za pokazanie na teamsach) za pomocą pętli/tablic oraz IFS

## Zadanie 2

Napisz skrypt generujący plik HTML z tabelą zawierającą wszystkich użytkowników opisanych w pliku /etc/passwd. Kolumny kluczowe to:

* login
* katalog domowy
* skonfigurowana powłoka domyślna (jedna z kolumn - postaraj się określić która)

Niech ten skrypt nie dołącza użytkowników bez skonfigurowanej powłoki (czyli ma ustawioną powłokę jako ```nologin``` albo ```false```)

## Zadanie 3

Napisz program wypisujący ramkę z tekstem o zadanych wymiarach i zadanym tekście w środku. Tekst do wypisania powinien być podany jako argument wywołania skryptu.

## Zadanie 4

Napisz skrypt generujący choinkę zadanej wysokości. (czyli klasyk).
Wysokość powinna zostać przekazana za pomocą argumentu skryptu. Niech skrypt ten będzie obsługiwał błędy, to znaczy - należy wyświetlić informację o tym, że należy podać argument w przypadku gdy nie został podany. Argumentem powinna być liczba.

## Zadanie domowe

Napisz skrypt wyświetlający drzewo katalogów tak jak komenda tree.

Skrypt powinien pozwalać na określenie głębokości zagnieżdżenia.

Dodatkowo proponuję pokolorować to drzewko.
