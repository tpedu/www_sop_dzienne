# 06 Zaawansowane skrypty

2020-04-09

## Funkcje

Bash obsługuje funkcje. Funkcję deklaruje się tak:

```bash
function witaj {
  echo "Funkcja ta jest nieuprzejma i nie wita $1"
  # tu opcjonalnie: return 0
}
```

Argumenty są liczone tak jak przy zwykłym skrypcie (```$1, $2, ...```).

Funkcję wywołuje się po prostu przez nazwę.

Jeśli chcesz zaimportować plik z funkcjami, to można to zrobić tak:

```bash
. ./plikDoZaimportowania
```

Tak właściwie, to taki plik zostanie dołączony w tym miejscu do bieżącego skryptu.

### Zadanie 0

1. Przygotuj skrypt z funkcją która będzie wypisywała tekst pokolorowany na zadany kolor.
   Niech funkcja ta  przyjmuje dwa argumenty - tekst i kolor. Zobacz ```man console_codes```.
   * rozwinięcie - kolor byłby określany tekstowo jako ```red```, ```green```, i tak dalej

dołącz ten skrypt za pomocą kropki, na przykład:

```bash
. ./skrypt
```

sprawdź czy wpisując nazwę funkcji kolorującej tekst w konsol, uzyskuje się pokolorowany tekst.


## sha256sum, md5sum

Na podstawie zawartości pliku można stworzyć sumę kontrolną. Do tego służożą komendy ```md5sum``` oraz ```sha256sum```. Wykonanie tych komend pdaje w efekcie dwie wartości - sumę kontrolną oraz nazwę pliku. Na przykład:

```bash
sha256sum input.txt
a472ed981b2effb0840ac4e11df232e77451584e42cb09c280c84cf7fb845dcc  input.txt
```

Lepiej korzystać z sha256sum, ponieważ daje znacznie mniejszą szansę na konflikt (2 różne argumenty funkcji hashującej a ta sama wartość).

## Zadanie 1

(uwaga - robiąc to zadanie zainspirowałem się sposobem działania git-a)

Można założyć, że konkretna suma kontrolna odnosi się do dokładnie jednej zawartości pliku
(jest to uproszczenie, ale szansa na konflikt jest absurdalnie niska). Tak więc majac
sumę kontrolną 2 plików można stwierdzić, czy są one takie same. Można z tego skorzystać w
celu zmniejszenia ilości danych w katalogu. Wystarczy zapisać jeden raz
zawartość pliku, a gdzieś obok przechować informację o tym jak się nazywają pliki o danej sumie kontrolnej.

Przykład takiego pliku:

```raw
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 ./bla
a472ed981b2effb0840ac4e11df232e77451584e42cb09c280c84cf7fb845dcc ./input.txt
11555555f1f02ff0da8b7b35abc828e6cda6abf1a8baa4d5efb25c67f65c1a15 ./bbb/inncos.txt
11555555f1f02ff0da8b7b35abc828e6cda6abf1a8baa4d5efb25c67f65c1a15 ./inne.txt
```

Napisz skrypt (wejście i wyjście może być jako argumenty z linii komend):

* niech ```INPUT``` oznacza katalog z którego będziemy brali pliki
* niech ```OUTPUT``` oznacza katalog do którego będziemy zapisywali wyniki
* skrypt wczyta listę plików z ```INPUT``` - pomijamy puste katalogi
* dla każdego pliku policzy sumę kontrolną
* wygeneruje plik z sumami kontrlnymi i nazwami plików, zapisze to do ```OUTPUT/lista.txt```
* skopiuje pliki z ```INPUT``` do ```OUTPUT/data``` w taki sposób, żeby pliki o tej samej zawartości były tylko raz (przypomnij sobie - co daje suma kontrolna)

W efekcie powinniśmy dostać katalog z plikami - każdy dla jednej sumy kontrolnej, oraz plik opisujący oryginalne nazwy plików i ich sumy kontrolne

Przykładowy katalog wejściowy [lab6inputex.tar.bz2](data/lab6inputex.tar.bz2)
Przykładowy wynik to [lab6example.tar.bz2](data/lab6example.tar.bz2)

## Zadanie 2

Napisz skrypt który odwróci proces z poprzedniego zadania, to znaczy odtworzy oryginalny układ katalogów i pliki (czyli też wilokrotnie pliki o tej samej zawartości).

Przykładowy katalog wejściowy [lab6example.tar.bz2](data/lab6example.tar.bz2)
Przykładowy wynik to [lab6inputex.tar.bz2](data/lab6inputex.tar.bz2)

## tar, bzip2, bunzip2, gzip, gunzip

Polecenia tar, bzip2, gzip, bunzip2 oraz gunzip służą do operacji na archiwach.

Program tar (Tape ARchiver) pierwotnie służył do tworzenia kopii zapasowych na taśmach
magnetycznych. Aktualnie jest raczej wykorzystywane do tworzenia zwykłych archiwów.
Może on korzystać także z programów do kompresji, czyli bzip2 oraz gzip.

Programy bzip2, gzip, bunzip2 oraz gunzip służą do kompresji
lub dekompresji pliku (pojedynczego). Wykorzystują różne algorytmy kompresji, bzip2 lepiej pakuje
w większości przypadków, ale za to działa wolniej niż gzip.

Najczęściej polecenie tar uruchamia się z następującymi parametrami:

```bash
tar [opcje] <nazwa>
```

Nazwa może odnosić się do jednego pliku, albo do katalogu, albo może to
być lista plików na których wykonamy operacje. Jeśli rozpakowujemy
archiwum, to nazwa powinna być pusta.
Opcje składa się tak jak w większości komend Linuxowych, czyli jeśli 
są to skróty jednoliterowe to można je sklejać. Opcje mogą wyglądać 
następująco (właściwie to wypisuję najczęściej używane kombinacje opcji,
zainteresowanych odsyłam do man):

* -v - verbose, czyli wyświetla co robi, można dać dwa razy 'v', aby wyświetlał jeszcze więcej informacji.
* -cf <nazwa> - tworzy plik o nazwie "nazwa". UWAGA: nadpisuje pliki bez pytania!
* -czf <nazwa> - tworzy plik o nazwie "nazwa". Plik ten jest kompresowany za pomocą gzip-a.
* -cjf <nazwa> - tworzy plik o nazwie "nazwa". Plik ten jest kompresowany za pomocą bzip2-a.
* -xf <nazwa> - rozpakowuje archiwum o nazwie "nazwa".
* -xzf <nazwa> - rozkompresowuje i rozpakowuje archiwum o nazwie "nazwa" które jest spakowane gzip-em. 
* -xjf <nazwa> - rozkompresowuje i rozpakowuje archiwum o nazwie "nazwa" które jest spakowane bzip2.

Przykłady to:

```bash
tar -czf obrazki1.tar.gz ./obrazki/*
tar -xzf obrazki1.tar.gz
```

Ciekawym poleceniem jest time, służy do liczenia ile czasu zajęło wykonanie
jakiejś komendy.

## Zadanie 3 (Albo Zadanie Domowe 1)

Rozwiń poprzednie zadanie o obsługę archiwów, to znaczy: przetworzone katalogi powinny być spakowane do archiwum tar.gz albo tar.bz2, a w drugą stronę takie archiwum powinno dać się rozpakować.

### wget

Program wget służy do pobierania plików ze zdalnych lokalizacji. Obsługuje on http, https, ftp i kilka innych protokołów. Przykład użycia:

```bash
wget "ftp://ftp.task.gda.pl/mirror/ftp.alsa-project.org/pub/driver/alsa-driver-0.4.0.tar.gz"
```

Ciekawsze opcje dostępne dla wget:

* -i <nazwapliku> - pobiera listę URL-i z pliku "nazwapliku".
* -r - pobiera całą witrynę wraz z podstronami.
* -l <liczba> - pobiera tylko tyle poziomów odnośników ile jest podane w "liczba" 
* -k - poprawia odnośniki w ściągniętych dokumentach, aby wskazywały do kopii lokalnej.
* --follow-ftp - ściąga także pliki z FTP, domyślnie nie robi tego.
* -A <maska> - ściąga tylko pliki spełniające wzorzec z "maska" - patrz wyżej
   (wildcard, maska ... - na przykład *.png).
* -R <maska> - ściąga tylko pliki nie spełniające wzorca "maska".

### Zadanie 4

Za pomocą komendy wget pobierz wszystkie pliki obrazków z wybranej przez Ciebie strony. Zakładamy że strona jest statyczna (nie ładująca obrazków za pomocą skrytpów JS tylko w postaci tagu ```img```)

### Zadanie domowe

Tu będą dwa skrypty.

Napisz skrypt który będzie pobierał co 10 sekund kurs jakiejś waluty lub kryptowaluty z zadanego źródła (wybierz - jakieś API albo coś takiego).

Napisz skrypt, który będzie generował wykres kursu z wykorzystaniem programu gnuplot (albo innego, jeśli masz takie preferencje).

### Zadanie domowe dodatkowe

(tego nie omawiałem, ale na dodatkowego plusa się nadaje)

Niech te skrypty działają jako usługa - do tego potrzeba uprawnień roota. Zobacz też komendę ```service```.
Niech co około 10 minut usługa ta wygeneruje w katalogu ```/tmp``` plik obrazka z wykresem.

### UWAGA - można dziś dostać 3 plusy z zadań domowych