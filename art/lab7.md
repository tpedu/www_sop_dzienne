# 07 Język C -- wprowadzenie

2020-04-16

Dzisiaj rozpoczynamy język C. Na początek podpowiedź:

```c
gcc nazwapliku.c -o nazwapliku -Wall
```

## Wstęp

Literatura:

* Brian W. Kernighan, Dennis M. Ritchie: Język ANSI C. Programowanie. Wydanie II

Co powinniśmy znać:

* ANSI C, C99, ...
* komentarze
* zmienne (zobacz też [typy danych](https://en.wikipedia.org/wiki/C_data_types) )
  * tablice statyczne
  * tablice dynamiczne
  * wskaźniki
  * struktury
  * typy wyliczeniowe (enum)
  * unie (union)
* arytmetyka
* funkcje
  * wskaźniki na funkcje
* instrukcje warunkowe
* pętle
* wielowybór

### Ważniejsze przykłady struktur

```c
union
{
    short int i;
    char j;
    struct
    {
        unsigned char lower;
        unsigned char higher;
    } s;
} u;
```

```c
struct s {
int a,b;
};
```

## 1 Zadania na początek

1. Napisz program obliczający sumę dowolnie wielu liczb wprowadzonych przez użytkownika i wypisujący wynik na standardowe wyjście. Przydatne hasła to:
    * scanf
    * printf
    * operatory arytmetyczne
    * if
    * for albo while
1. Przerób ten program tak, aby wartość była zwracana jako kod zakończenia programu. Wyświetl tą wartość z linii komend. Przydatne komendy:
    * return
1. Napisz program wyświetlający tradycyjną choinkę o zadanej wysokości. Wysokość niech będzie podana jako parametr wywołania programu. Przydatne hasła to:
    * atoi
    * argc, argv
    * for

## 2 Zadania z IO

### IO 1

Napisz program wczytujący z pliku tekstowego ciąg liczb. Format pliku jest taki:

1. liczba elementów ciągu
1. Kolejne liczby

Niech program wypisze je w odwrotnej kolejności.

Skorzystaj z fopen, fclose, fscanf.

### IO 2

Napisz program, który będzie pozwalał na wyświetlenie dowolnego pliku w postaci takiej, jak w
heksedytorach. Program powinien wypisać zawartość pliku w postaci liczb szesnastkowych
będących wartościami kolejnych bajtów tego pliku. Warto zrobić tak, aby kolejne wartości
były pogrupowane po 8 albo po 4 w celu poprawy czytelności. 

## O wskaźnikach

Przy zmiennych lub wartościach:

* Gwiazdka ```*``` oznacza pobranie wartości wskazywanej przez dany adres (wartość przy gwiazdce traktujemy jako adres).
* Znak ```&``` oznacza pobranie adresu danej zmiennej

Przy deklaracji zmiennych:

* Gwiazdka oznacza typ wskaźnikowy - uwaga - tych gwiazdek może być więcej niż jedna
* Można zadeklarować wskaźnik na funkcję, wtedy robi się to tak:
  * ```typ_zwracany (*nazwa_funkcji)(typy_argumentow,...);```

```c
int z = 12;
int *y;
int **x;

y = &z;
x = &y;
// **x == 12; *y == 12; Nie wiadomo przed uruchomieniem jaka będzie wartość x oraz y. Dlaczego?
```

### Zadania

1. Napisz program z 3 funkcjami, każda funkcja niech będzie miała taką sygnaturkę:
   * int (*f)(int,int)
 Niech będą to funkcje: maksimum, minimum oraz suma.
1. Zapamiętaj wskaźniki do tych funkcji w tablicy trójelementowej
1. Niech program pobiera kolejno wartości:
   1. liczba \\(z\\) od 0 do 2 oznaczająca indeks funkcji w tablicy
   1. liczba elementów ciągu do pobrania (nazwijmy ją \\(n\\) )
   1. kolejno \\(n\\) liczb
1. Niech program przetwarza wczytaną tablicę w taki sposób, że:
    1. Niech \\(v\\) przyjmie wartość pierwszego elementu
    1. w pętli po wszystkich elementach od drugiego (indeks 1 w tablicy)
       * wykona funkcję o numerze \\(z\\) przekazując jako argumenty \\(v\\) oraz element o aktualnym indeksie. Niech wynik zostanie zapisany do \\(v\\)
    1. Wypisze wartość \\(v\\)

Czyli piszemy program który pozwoli na wykonanie albo sumy, albo znalezienia maksimum, albo znalezienia minimum z \\(n\\) liczb. Program nie będzie korzystał z instrukcji warunkowych (w pętli głównej) do wyboru jaka funkcja ma być wykonana.

Korzystając z wiedzy z poprzedniego zadania, napisz funkcję, która będzie przetwarzała tablicę za pomocą uniwersalnej funkcji podanej jako argument funkcji. Na przykład:

```c
int wynik = forAll(tablica, liczbaElementow, maksimum); // maksimum to nazwa funkcji porownojacej dwie wartosci
```

### Zadanie Domowe

1. Napisz program implementujący listę dwustronnie wiązaną.
1. Napisz program liczący kolejne wartości ciągu Fibonacciego w wersji:
    * rekursywnej (rekurencyjnej)
    * iteracyjnej
