# Laboratorium 8

2020-04-23

## Fork

Komendy: fork, wait, getpid, getppid

### Zadania

1. Napisz program który wyświetli jaki ma PID oraz PPID. Poczeka 5 sekund i się zakończy. Zobacz za pomocą komendy ps czy faktycznie te wartości się zgadzają.
1. Napisz program, który uruchomi podproces za pomocą komendy fork. Niech proces rodzica oczekuje na proces potomka. Oba procesy powinny wypisać swój PID oraz PPID.
1. Napisz program, który uruchomi 100 procesów potomnych w taki sposób, że będzie jeden rodzic i 100 potomków. Proces główny niech oczekuje na zakończenie wszystkich procesów potomnych.
1. Napisz program, który uruchomi 100 procesów w taki sposób, że każdy kolejny potomek stworzy potomka. Niech każdy proces poczeka na zakończenie procesu potomka.
1. Napisz prosty program chat-u, który będzie korzystał z pliku w katalogu /tmp do przekazywania wiadomości. Zastanów się jak to zrobić. Zachęcam zapytać prowadzącego o podpowiedzi.

### Uwaga

Uruchomienie programu i przechwycenie jego standardowego wyjścia: [przykład na Stacku](http://stackoverflow.com/questions/646241/c-run-a-system-command-and-get-output)

Pobranie wymiarów terminala:

```c
tput cols
tput lines
```

## Zadanie domowe

Rozwiń program chat-u tak, aby program ten wyglądał podobnie do:

```raw
+-----------------------------------+
|alice: czesc                       |
|bob: no siema                      |
|                                   |
|                                   |
|                                   |
|                                   |
+-----------------------------------+
|bob: tu wpisujemy_                 |
+-----------------------------------+
```

Program powinien się zakończyć w momencie gdy użytkownik zakończy strumień wejściowy (Ctrl+D).
