# 09 Make

2020-04-30

Program make, obsługa plików i dyunamiczna alokacja pamięci.

## Zadania

Tym razem bez większego wstępu.

### Zadanie 1

Napisz program sprawdzający rozmiar pliku. (to może być zadanie dla prowadzącego, albo dla osoby chętnej).

Funkcje związane z odczytem pliku to ```fopen```, ```fread```, ```fscanf```, ```fclose```, ```fseek```.

### Zadanie 2

Napisz program ładujący do pamięci cały plik tekstowy podany jako parametr uruchomienia programu.

Przyda się funkcja malloc, prawdopodobnie przyda się także realloc.

### Zadanie 3

Tu przydadzą się komendy malloc oraz ```free```.

Teraz trochę kombinowania. Mamy plik tekstowy, zawierający listę plików (na przykład graficznych - rozszerzenia dowolne).
Lista ta jest rozdzielona znakami nowej linii. Nazwy plików nie zawierają spacji. Jeśli zrobisz z obsługą dowolnie długich nazw plików to masz dodatkowy plus (proszę się zgłosić).

Napisz program, który wczyta dowolnie długą taką listę z pliku.

Podpowiem -- będzie potrzebna tablica wskaźników. Taka lista nazw będzie ```char **```.

### Zadanie 4

Niech program będzie wypisywał posortowaną listę plików z zadania poprzedniego. Zastosuj najprostszy algorytm sortowania jaki sobie wymyślisz. Jeśli nie masz pomysłu na porównywanie nazw dwóch plików, to wystarczy porównywanie pierwszego znaku z nazwy.

### Zadanie 5

Rozbij zadanie poprzednie  na moduły:

* moduł z funkcją main
* moduł zawierający funkcję ładowania listy plików do pamięci
* moduł służący do sortowania elementów tablicy

Pamiętaj o plikach nagłówkowych.

Przetestuj kompilację za pomocą:

```bash
gcc -c nazwapliku.c
..
gcc nazwapliku.o nazwapliku2.o ... -o nazwaprogramu
```

oraz za pomocą:

```bash
gcc nazwapliku.c nazwapliku2.c ... -o nazwaprogramu
```

zastanów się, jakie są wady i zalety tych podejść

### Zadanie 6

Przygotuj prosty plik Makefile, który będzie automatyzował proces kompilacji tego programu.

Wykonaj komendę make -- powinno się wszystko skompilować.

Teraz zmodyfikuj tylko plik zawierający funkcję main.

Znowu wykonaj make. Jeśli wszystko się udało, to powinien się skompilować tylko plik zmodyfikowany, oraz powinna nastąpić konsolidacja (linkowanie).
Jeśli tak nie zadziałało -- poproś prowadzącego o pomoc (**KONIECZNIE**).

### Zadanie 7

Zmodyfikuj projekt (oraz oczywiście plik Makefile) tak, aby:

* pliki ```.c``` (źródłowe) znajdowały się w katalogu src
* pliki ```.h``` (nagłowkowe) znajdowały się w katalogu inc
* pliki ```.o``` były tworzone w katalogu build
* plik wykonywalny pojawiał się w bieżącym katalogu
* działał cel ```clean```, czyszczący pliki tymczasowe i skompilowane

### Zadanie domowe

Zmodyfikuj Makefile w taki sposób, aby nie trzeba było podawać listy plików do skompilowania ręcznie,
tylko aby wszystkie pliki .c z katalogu ```src``` były kompilowane i linkowane. Pozwól na to, aby można było podmienić flagi kompilacji (tradycyjnie ```CFLAGS```) poprzez ustawienie
zmiennej środowiskowej ```CFLAGS```. Niech domyślnie ```CFLAGS=-Wall```.
