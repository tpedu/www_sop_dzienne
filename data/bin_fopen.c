#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

typedef struct person_t {
	int yob;
	char name[16];
} person_t;

int main(int argc, char **argv) {
	if (argc < 3) return -1;
	if (argv[1][0] == 'r') {
		FILE *ff = fopen(argv[2],"rb");
		person_t person;
		fread(&person,sizeof(person),1,ff);
		printf("%d %s\n", person.yob, person.name);
		fclose(ff);
	} else {
		FILE *ff = fopen(argv[2],"wb");
		person_t person;
		person.yob = 1920;
		strcpy(person.name,"Janusz");
		fwrite(&person,sizeof(person),1,ff);
		fclose(ff);
	}
	return 0;
}
