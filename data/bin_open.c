#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

typedef struct person_t {
	int yob;
	char name[16];
} person_t;

int main(int argc, char **argv) {
	if (argc < 3) return -1;
	if (argv[1][0] == 'r') {
		int f = open(argv[2], O_RDONLY);
		person_t person;
		read(f, &person,sizeof(person));
		printf("%d %s\n", person.yob, person.name);
		close(f);
	} else {
		int f = open(argv[2],O_WRONLY|O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		person_t person;
		person.yob = 1920;
		strcpy(person.name,"Janusz");
		write(f, &person,sizeof(person));
		close(f);
	}
	return 0;
}
