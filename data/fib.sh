#!/bin/bash

function f () {
	local N=$1
	if (($N<=1)); then return 1; fi
	f $((N-1))
	local RET1=$?
	f $((N-2))
	local RET2=$?
	return $((RET1+RET2))
}

f $1
echo "wynik $?"
