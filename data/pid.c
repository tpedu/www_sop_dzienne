#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

int main () {
  FILE *f = fopen("dane.txt","w+");
  if (f == NULL) {
      fprintf(stderr, "Nie moglem otworzyc pliku!\n");
      return EXIT_FAILURE;
  }

  int pid = fork(); // <0 error
  char tekst[101];

  if (pid < 0) {
      fprintf(stderr, "Nie udalo sie utworzyc procesu!\n");
      return EXIT_FAILURE;
  }
  if (pid > 0) { // rodzic !
    printf("parent :D pid: %d; potomek %d\n",getpid(), pid);  
    printf("parent :D ppid: %d\nczekamy...\n",getppid());
    wait(NULL);
    fseek(f, 0, SEEK_SET);
    fscanf(f,"%s",tekst);
    printf("parent :D ppid: %d\nparent tekst: %s\n",getppid(),tekst);
  } else { // dziecko (proces potomny)
    scanf("%100s",tekst);
    fprintf(f,"%s",tekst);
    printf("child :D pid: %d\n",getpid());  
    printf("child :D ppid: %d\ntekst: %s\n",getppid(),tekst);
  }
  fclose(f);
  return EXIT_SUCCESS;  
}