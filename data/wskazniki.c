#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void wypiszcos() {
    char tekst[] = "wypisujemy";
    printf("---%s---\n", tekst);
}

void oblicz_abc(char *a, int *b, float *c) {
    *a = 1;
    *b = -2;
    *c = 12.45;
}

int main(int argc, char **argv) {
    int liczba;
    liczba = 0x01234;

    printf("Liczba: 0x%x\n",liczba); 
    printf("Adres liczby: 0x%x\n",&liczba);
    printf("Pierwszy bajt liczby: 0x%x\n",*(unsigned char *)&liczba);
    printf("Kolejny bajt liczby: 0x%x\n",*((unsigned char *)&liczba + 1));
    printf("Kolejny bajt liczby: 0x%x\n",*((unsigned char *)&liczba + 2));
    printf("Kolejny bajt liczby: 0x%x\n",*((unsigned char *)&liczba + 3));

    printf("[0]: 0x%x\n",((unsigned char *)&liczba)[0]);
    printf("[1]: 0x%x\n",((unsigned char *)&liczba)[1]);

    unsigned char tablica_charow[4];
    for (int i = 0; i < 4; i++) tablica_charow[i] = i+1;
    printf("tablica_charow[1]: 0x%x\n",tablica_charow[1]);
    printf("tablica_charow[1]: 0x%x\n",*(tablica_charow+1));
    printf("tablica_charow[0]: 0x%x\n",*tablica_charow);

    printf("tablica_charow jako liczba unsigned int: 0x%x\n",*(unsigned int *)tablica_charow);

    char a;
    int b;
    float c;
    oblicz_abc(&a, &b, &c);

    printf("%d %d %f %d\n", a, b, c, 888);


    char *tekst = (char *)malloc(100);

    free(tekst);


    char bla[] = "Tekst do wypisania";
    for (int i = 0; i <= strlen(bla); i++) {
        printf("%x(%c) ", bla[i], bla[i]);
    }
    printf("\n");

    for (int i = 0; i < argc; i++) {
        char *arg = argv[i];
        printf ("argument %d = %s :  %c %c\n", i, arg, *arg, **(argv+i)); // char *
    }
    {
    char *a = (char *)wypiszcos;
    for (int i = 0; i < 200; i++) {
        printf("%c",a[i]);
    }
    printf("\n");
    }
    return 0;
}
