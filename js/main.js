


var f = function() {

var converter = new showdown.Converter();
converter.setOption('literalMidWordUnderscores', 'true');
converter.setOption('literalMidWordAsterisks', 'true');
converter.setOption('tables', 'true');
showdown.setFlavor('github');

  var articlesJson = {};

  console.log("Loading page...");


  var loadMarkdownFromUrl =
      function(e, url) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      var DONE = 4;  // readyState 4 means the request is done.
      var OK = 200;  // status 200 is a successful return.
      if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
          e.innerHTML = converter.makeHtml(xhr.responseText);
          $('pre code')
              .each(function(i, block) { 
                hljs.highlightBlock(block); 
              });
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        } else {
          console.log(
              'Error: ' + xhr.status);  // An error occurred during the request.
        }
      }
    };
    xhr.open('GET', url);
    xhr.send(null);
  }

  var loadDocumentsList =
      function(elementid, urlAddr, callbackf) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      var DONE = 4;  // readyState 4 means the request is done.
      var OK = 200;  // status 200 is a successful return.
      var elementToAdd = document.getElementById(elementid);
      if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
          var jj = JSON.parse(xhr.responseText);
          console.log(jj);
          articlesJson= jj;
          document.getElementById("labnamefull").innerHTML = jj.section.name;
          document.title = jj.section.name;
          var listhtml = "";
          for (i = 0; i < jj.article.length; i++) {
             var art = jj.article[i];
             //loadMarkdownFromUrl(e, e.attributes['data-src'].value);
             //console.log('Loading ' + e.attributes['data-src'].value);
             console.log(art.name + " " + art.markdown);
             listhtml = listhtml+`<div class="col-lg-6">
                <h4>${art.name}</h4>
                <p>${art.description} <a id="art${i}" href="#art/${art.markdown}">Dalej >></a>.</p>
              </div>`;
           }
           elementToAdd.innerHTML = listhtml;
           for (i = 0; i < jj.article.length; i++) {
            var art = jj.article[i];
            var e = document.getElementById(`art${i}`);
            e.onclick = function(evnt) {
              var artname = evnt.target.href.substring(evnt.target.href.indexOf('#')+1 );
              console.log(evnt);
//              document.getElementById("mdtarget")
              console.log(artname);
              loadMarkdownFromUrl(document.getElementById("mdtarget"),artname);
              document.title = articlesJson.section.name + " - " + artname;
            };
           }
        } else {
          console.log(
              'Error: ' + xhr.status);  // An error occurred during the request.
        }
      }
      if (callbackf) callbackf(xhr);
    };
    xhr.open('GET', urlAddr);
    xhr.send(null);
  }

    var articlename=window.location.href.substring(window.location.href.indexOf('#')+1 );

    if (window.location.href.indexOf('#') <= 0) articlename = "art/intro.md";
    loadMarkdownFromUrl(document.getElementById("mdtarget"),articlename);
    // load list of articles and present them on page:
    loadDocumentsList("sectionslist", "art/articles.json"); 

};

document.onreadystatechange = f;
